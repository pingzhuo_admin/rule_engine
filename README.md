# rule_engine

#### 介绍
基于drools的规则引擎项目，通过动态配置生成drl文件然后执行对应的规则，可以通过页面[web平台管理](https://gitee.com/gz_miniproject/rule_engine_admin)， 进行相关参数配置；目的是做成一个独立的平台。

#### 软件架构
基于SpringBoot、Mybatis、Drools的一个规则引擎后台管理系统
项目目录：
- rule-admin 主要是登录、系统设置相关以及db脚本；
- rule-common  公共；
- rule-drools  规则相关；
- rule-front  对外提供api；


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
