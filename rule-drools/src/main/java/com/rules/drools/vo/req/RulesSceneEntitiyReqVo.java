package com.rules.drools.vo.req;

import lombok.Data;

import java.util.List;

/**
 * ClassName: RulesSceneEntitiyReqVo <br/>
 * Description: 场景实体 <br/>
 * date: 2019/7/12 16:05<br/>
 * @author gz<br />
 * @since JDK 1.8
 */
@Data
public class RulesSceneEntitiyReqVo {
    private Long sceneId;
    /**
     * 标识
     */
    private String sceneIdentify;
    /**
     * 类型(暂不使用)
     */
    private Integer sceneType;
    /**
     * 名称
     */
    private String sceneName;
    /**
     * 描述
     */
    private String sceneDesc;
    /**
     * 是否有效
     */
    private Integer isEffect;

    /**
     * 备注
     */
    private String remark;
    /**
     * 关联实体id
     */
    private List<Long> entityId;


}
