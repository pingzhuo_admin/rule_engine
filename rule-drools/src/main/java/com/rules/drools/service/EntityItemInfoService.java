package com.rules.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import com.rules.drools.dao.EntityItemInfoDao;
import com.rules.drools.entity.EntityItemInfoEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("entityItemInfoService")
public class EntityItemInfoService extends ServiceImpl<EntityItemInfoDao, EntityItemInfoEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<EntityItemInfoEntity> page = this.page(
                new Query<EntityItemInfoEntity>().getPage(params),
                new QueryWrapper<EntityItemInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * <p>
     * 方法说明: 根据实体id获取规则引擎实体属性信息
     *
     * @param baseRuleEntityItemInfo 参数
     */
  public   List<EntityItemInfoEntity> findBaseRuleEntityItemInfoList(EntityItemInfoEntity baseRuleEntityItemInfo){
        return this.baseMapper.findBaseRuleEntityItemInfoList(baseRuleEntityItemInfo);
    }

    /**
     * <p>
     * 方法说明: 根据id获取对应的属性信息
     *
     * @param id 属性Id
     */
    public  EntityItemInfoEntity findBaseRuleEntityItemInfoById(final Long id){
        return this.baseMapper.findBaseRuleEntityItemInfoById(id);
    }

    public void removeByEntityId(Long entityId){
        LambdaUpdateWrapper<EntityItemInfoEntity> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(EntityItemInfoEntity::getEntityId,entityId);
        this.remove(wrapper);
    }


}
