package com.rules.drools.service;

import com.rules.drools.vo.fact.RuleExecutionObject;

import java.util.Map;

/**
 * 描述： drools 实现类动作接口
 */
public interface DroolsActionService {

    /**
     * <p>
     * 方法说明: 引擎执行结束,实现类默认执行方法
     *
     * @param fact   参数
     * @param result 结果集
     */
    void execute(RuleExecutionObject fact, Map<String, Object> result);


}
