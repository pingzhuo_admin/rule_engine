package com.rules.drools.service;

import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import com.rules.drools.dao.SceneEntityRelDao;
import com.rules.drools.entity.EntityInfoEntity;
import com.rules.drools.entity.SceneEntityRelEntity;
import com.rules.drools.entity.SceneInfoEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("sceneEntityRelService")
public class SceneEntityRelService extends ServiceImpl<SceneEntityRelDao, SceneEntityRelEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SceneEntityRelEntity> page = this.page(
                new Query<SceneEntityRelEntity>().getPage(params),
                new QueryWrapper<SceneEntityRelEntity>()
        );

        return new PageUtils(page);
    }



    /**
     * <p>
     * 方法说明: 根据场景信息获取相关的实体信息
     *
     * @param baseRuleSceneInfo 参数
     */
  @Cached(name="entityInfoEntity_findBaseRuleEntityListByScene",cacheType= CacheType.LOCAL,key="#baseRuleSceneInfo.sceneIdentify", expire = 60)
  public  List<EntityInfoEntity> findBaseRuleEntityListByScene(SceneInfoEntity baseRuleSceneInfo){
      return this.baseMapper.findBaseRuleEntityListByScene(baseRuleSceneInfo);
  }

}
