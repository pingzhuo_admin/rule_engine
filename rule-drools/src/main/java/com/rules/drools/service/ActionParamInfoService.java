package com.rules.drools.service;

import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import com.rules.drools.dao.ActionParamInfoDao;
import com.rules.drools.entity.ActionParamInfoEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("actionParamInfoService")
public class ActionParamInfoService extends ServiceImpl<ActionParamInfoDao, ActionParamInfoEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ActionParamInfoEntity> page = this.page(
                new Query<ActionParamInfoEntity>().getPage(params),
                new QueryWrapper<ActionParamInfoEntity>()
        );

        return new PageUtils(page);
    }


    /**
     *
     * 方法说明: 获取动作参数信息
     * @param baseRuleActionParamInfo 参数
     */
    @Cached(name="actionParamInfoEntity_findBaseRuleActionInfoList",cacheType= CacheType.LOCAL, key="#baseRuleActionParamInfo.actionId", expire = 60)
    public List<ActionParamInfoEntity> findBaseRuleActionParamInfoList(ActionParamInfoEntity baseRuleActionParamInfo){
        return this.baseMapper.findBaseRuleActionParamInfoList(baseRuleActionParamInfo);
    }

    /**
     *
     * 方法说明: 根据动作id获取动作参数信息
     * @param actionId 参数
     */
    @Cached(name="actionParamInfoEntity_findRuleActionParamByActionId",cacheType= CacheType.LOCAL, key="#actionId", expire = 60)
    public List<ActionParamInfoEntity> findRuleActionParamByActionId( Long actionId){
        return this.baseMapper.findRuleActionParamByActionId(actionId);
    }


}
