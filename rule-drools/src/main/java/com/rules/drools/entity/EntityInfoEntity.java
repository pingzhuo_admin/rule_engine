package com.rules.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 规则引擎实体信息表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_entity_info")
public class EntityInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long entityId;
	/**
	 * 名称
	 */
	private String entityName;
	/**
	 * 描述
	 */
	private String entityDesc;
	/**
	 * 标识
	 */
	private String entityIdentify;
	/**
	 * 包路径
	 */
	private String pkgName;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 是否有效(1是0否)
	 */
	private Integer isEffect;
	/**
	 * 备注
	 */
	private String remark;


	/**
	 * 方法说明:获取实体类名称
	 */
	public String getEntityClazz() {
		int index = pkgName.lastIndexOf('.');
		return pkgName.substring(index + 1);
	}

}
