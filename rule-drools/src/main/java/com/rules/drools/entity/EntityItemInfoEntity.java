package com.rules.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体属性信息
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_entity_item_info")
public class EntityItemInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long itemId;
	/**
	 * 实体id
	 */
	private Long entityId;
	/**
	 * 字段名称
	 */
	private String itemName;
	/**
	 * 字段标识
	 */
	private String itemIdentify;
	/**
	 * 属性描述
	 */
	private String itemDesc;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 备注
	 */
	private String remark;

}
