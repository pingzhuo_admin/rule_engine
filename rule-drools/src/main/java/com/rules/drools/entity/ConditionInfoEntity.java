package com.rules.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 规则条件信息表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:39
 */
@Data
@TableName("rule_condition_info")
public class ConditionInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long conditionId;
	/**
	 * 规则
	 */
	private Long ruleId;
	/**
	 * 条件名称
	 */
	private String conditionName;
	/**
	 * 条件表达式
	 */
	private String conditionExpression;
	/**
	 * 条件描述
	 */
	private String conditionDesc;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 备注
	 */
	private String remark;

}
