package com.rules.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rules.drools.entity.ActionRuleRelEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 动作与规则信息关系表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface ActionRuleRelDao extends BaseMapper<ActionRuleRelEntity> {
	
}
