package com.rules.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.rules.admin.entity.SysLogEntity;
import com.rules.core.utils.PageUtils;

import java.util.Map;


/**
 * 系统日志
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
