package com.rules.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rules.admin.dao.NoticeDao;
import com.rules.admin.entity.NoticeEntity;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class NoticeService extends ServiceImpl<NoticeDao, NoticeEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<NoticeEntity> page = this.page(
                new Query<NoticeEntity>().getPage(params),
                new QueryWrapper<NoticeEntity>()
        );

        return new PageUtils(page);
    }

}
