package com.rules.admin.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DozerBeanMapperConfigure {
    @Bean
    public DozerBeanMapper getSingletonDozerBeanMapper() {
        DozerBeanMapper mapper = new DozerBeanMapper();
        return mapper;
    }

}
