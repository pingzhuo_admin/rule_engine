package com.rules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统公告
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-22 14:19:18
 */
@Data
@TableName("sys_notice")
public class NoticeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId
	private String id;
	/**
	 * 公告标题
	 */
	private String noticeTitle;
	/**
	 * 公告内容
	 */
	private String noticeContent;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建时间
	 */
	private Date creatTime;
	/**
	 * 修改人
	 */
	private String updater;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
