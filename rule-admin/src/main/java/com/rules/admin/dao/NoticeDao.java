package com.rules.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rules.admin.entity.NoticeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统公告
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-22 14:19:18
 */
@Mapper
public interface NoticeDao extends BaseMapper<NoticeEntity> {
	
}
