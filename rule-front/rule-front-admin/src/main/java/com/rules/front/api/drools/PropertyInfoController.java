package com.rules.front.api.drools;

import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Result;
import com.rules.drools.entity.PropertyInfoEntity;
import com.rules.drools.service.PropertyInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 规则基础属性信息表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@RestController
@RequestMapping("rules/propertyinfo")
public class PropertyInfoController {
    @Autowired
    private PropertyInfoService propertyInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = propertyInfoService.queryPage(params);

        return Result.ok().put("page", page);
    }

    /**
     * 下拉
     * @return
     */
    @RequestMapping("/select")
    public Result selectList(){
        return propertyInfoService.listProperty();
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{rulePropertyId}")
    public Result info(@PathVariable("rulePropertyId") Long rulePropertyId){
		PropertyInfoEntity propertyInfo = propertyInfoService.getById(rulePropertyId);

        return Result.ok().put("propertyInfo", propertyInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody PropertyInfoEntity propertyInfo){
		propertyInfoService.save(propertyInfo);

        return Result.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Result update(@RequestBody PropertyInfoEntity propertyInfo){
		propertyInfoService.updateById(propertyInfo);

        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody Long[] rulePropertyIds){
		propertyInfoService.removeByIds(Arrays.asList(rulePropertyIds));

        return Result.ok();
    }

}
